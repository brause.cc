// https://css-tricks.com/simple-css-row-column-highlighting/
if (screen.width < 600) {
  document.querySelector('body').setAttribute('class', 'nohover');
  document.querySelectorAll('td, th').forEach(function(el) {
    el.setAttribute('tabindex', '1');
    el.addEventListener('touchstart', function() {
      el.focus();
    });
  });
}

window.toggleHiddenRows = (function(){
  var rowsHidden = true;
  return function(e) {
    if (rowsHidden) {
      document.querySelectorAll('tr.hidden').forEach(function(el) {
        el.classList.replace('hidden', 'subdued');
      });
    } else {
      document.querySelectorAll('tr.subdued').forEach(function(el) {
        el.classList.replace('subdued', 'hidden');
      });
    }
    rowsHidden = !rowsHidden;
  }
})();
