#!/bin/bash
status=0

fail() {
    echo "failed $1" 1>&2
    status=1
}

verify() {
    emacs --batch --insert="$1" \
          --eval '(or (numberp (car (read (current-buffer)))) (exit 1))'
}

fetch_archive() {
    mkdir -p "$1"
    curl -sf -o "$1/archive-contents" "$2" || fail "fetching $1"
    if [[ -n $(git status -s -- "$1/archive-contents") ]]; then
        if verify "$1/archive-contents"; then
            git add "$1/archive-contents"
            git commit -m "AUTOCOMMIT $1" >/dev/null
        else
            cp "$1/archive-contents" "$1/$(date +%F_%T)"
            git restore "$1/archive-contents"
            fail "verifying $1"
        fi
    fi
}

cd "${GIT_REPO:-/srv/git/elpa}"

fetch_archive gnu "https://elpa.gnu.org/packages/archive-contents"
fetch_archive nongnu "https://elpa.nongnu.org/nongnu/archive-contents"
fetch_archive melpa "https://melpa.org/packages/archive-contents"
fetch_archive melpa-stable "https://stable.melpa.org/packages/archive-contents"

exit $status
