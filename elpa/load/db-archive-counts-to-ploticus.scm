(module db-archive-counts-to-ploticus ()

(import scheme)
(import (chicken base))
(import (chicken format))
(import (chicken process-context))
(import (chicken string))
(import (srfi 1))
(import scsh-process)
(import sql-de-lite)

(define stderr (current-error-port))
(define str string-append)

(define archive-names '("melpa" "melpa-stable" "gnu" "nongnu"))

(define (archive-counts db)
  (query fetch-rows
         (sql db "SELECT date,
                         GROUP_CONCAT(archive),
                         GROUP_CONCAT(package_count)
                    FROM archive_counts
                   GROUP BY date
                   ORDER BY date")))

(define (row->ploticus-row row)
  (let* ((date (car row))
         (archives (string-split (cadr row) ","))
         (counts (string-split (list-ref row 2) ","))
         (meta (map cons archives counts))
         (fields (map (lambda (name) (alist-ref name meta equal? ""))
                      archive-names)))
    ;; trailing empty field required, so no string-intersperse
    (apply str (map (lambda (field) (str field ","))
                    (cons date fields)))))

(define (ploticus-args archives)
  `("-prefab" "chron" "data=stdin" "delim=comma"
    "datefmt=yyyy-mm-dd" "mode=line" "legend=max+0.5 max"
    "x=1" "y=2" "y2=3" "y3=4" "y4=5"
    "yinc=500" "title=ELPA package counts"
    "xrange=2021-02-27 2022-03-28"
    ,@(map (lambda (label name) (format "~a=~a" label name))
           '("name" "name2" "name3" "name4")
            archives)))

;; | repo           | monthly | yearly | all-time |
;; |----------------+---------+--------+----------|
;; | melpa          |    x    |   x    |     x    |
;; | melpa-stable   |    x    |   *    |     *    |
;; | gnu            |    x    |   x    |     x    |
;; | nongnu         |    x    |   x    |     x    |
;; | melpa + stable |    x    |   *    |     *    |
;; | gnu + nongnu   |    x    |   x    |     x    |
;; | all repos      |    x    |   *    |     *    |

;; NOTE: there isn't enough data on melpa-stable, so it and all
;; derived combinations will have to wait
;; NOTE: scripts should be used with a minimum of data points
;; NOTE: stats should live in a separate directory
;; TODO: require a time range argument
;; TODO: require an archives argument

(define main
  (case-lambda
   ((db-path image-path . archives)
    (call-with-database db-path
      (lambda (db)
        (let* ((plot-rows (map row->ploticus-row (archive-counts db)))
               (stdin (string-intersperse plot-rows "\n")))
          (run (ploticus "-png" "-o" ,image-path ,@(ploticus-args archives))
               (<< ,stdin))))))
   (_
    (fprintf stderr "usage: ~a <db> <image> <archive> ...\n~a\n"
             (program-name) archive-names)
    (exit 1))))

(apply main (command-line-arguments))

)
