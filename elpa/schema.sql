CREATE TABLE IF NOT EXISTS packages(
  id INTEGER PRIMARY KEY,
  archive TEXT,
  name TEXT,
  version TEXT,
  desc TEXT,
  url TEXT,
  keywords TEXT,
  added INTEGER,
  permalink TEXT
);

CREATE TABLE IF NOT EXISTS archive_counts(
  id INTEGER PRIMARY KEY,
  archive TEXT,
  date TEXT,
  package_count INTEGER,
  UNIQUE(archive, date)
);
