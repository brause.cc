((gnu
  (title . "GNU ELPA")
  (archive-contents . "https://elpa.gnu.org/packages/archive-contents")
  (permalink . "https://elpa.gnu.org/packages/~a.html")
  (home-page . "https://elpa.gnu.org/"))
 (nongnu
  (title . "NonGNU ELPA")
  (archive-contents . "https://elpa.nongnu.org/nongnu/archive-contents")
  (permalink . "https://elpa.nongnu.org/nongnu/~a.html")
  (home-page . "https://elpa.nongnu.org/"))
 (marmalade
  (title . "Marmalade")
  (archive-contents . "https://marmalade-repo.org/packages/archive-contents")
  (permalink . "https://marmalade-repo.org/packages/~a")
  (home-page . "https://marmalade-repo.org/")
  ;; expired certificate
  (disable-verification? . #t))
 (melpa
  (title . "MELPA")
  (archive-contents . "https://melpa.org/packages/archive-contents")
  (permalink . "https://melpa.org/#/~a")
  (home-page . "https://melpa.org/"))
 (melpa-stable
  (title . "MELPA Stable")
  (archive-contents . "https://stable.melpa.org/packages/archive-contents")
  (permalink . "https://stable.melpa.org/#/~a")
  (home-page . "https://stable.melpa.org/")))
