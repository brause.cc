(module benchmark ()

(import scheme)
(import (chicken base))
(import (chicken format))
(import (chicken process-context))
(import micro-benchmark)

;; adapted from openssl
(define-values (open-connection close-connection call-with-transaction
                                query-filters query-species)
  (let ((db (string->symbol (or (get-environment-variable "DB") "json"))))
    (case db
      ((json)
       (import (wald json))
       (values open-connection close-connection call-with-transaction
               query-filters query-species))
      ((sqlite)
       (import (wald sqlite))
       (values open-connection close-connection call-with-transaction
               query-filters query-species))
      ((nstore)
       (import (wald nstore))
       (values open-connection close-connection call-with-transaction
               query-filters query-species))
      (else
       (error "Unknown DB" db)))))

(define connection-options '((read-only? . #t)))
(define filters
  '(("ecologicaltype" "saprotrophic")
    ("stipecharacter" "bare")
    ("stipecharacter" "na")
    ("capshape" "flat")
    ("capshape" "convex")
    ("hymeniumtype" "gills")
    ("whichgills" "adnate")
    ("whichgills" "subdecurrent")))

(define-syntax bench
  (syntax-rules ()
    ((bench prefix body ...)
     (fprintf (current-error-port) "~a: ~aμs\n" prefix
              (alist-ref 'mean (benchmark-run (10) body ...))))))

(define (main db-path)
  (let ((conn (open-connection db-path connection-options)))
    (call-with-transaction conn
      (lambda (tx)
        (for-each print (query-species tx filters))
        (bench "Filters" (query-filters tx))
        (bench "Query stats" (query-species tx filters))))
    (close-connection conn)))

(apply main (command-line-arguments))

)
