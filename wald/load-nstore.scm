(module load-nstore ()

(import scheme)
(import (chicken base))
(import (chicken process-context))
(import (srfi 1))
(import (srfi 167 engine))
(import (only (srfi 167 lmdb) make-default-engine))
(import (srfi 168))
(import medea)

(define (symbol->integer x)
  (string->number (symbol->string x)))

(define (vector-for-each proc vec)
  (let ((len (vector-length vec)))
    (let loop ((i 0))
      (when (< i len)
        (proc (vector-ref vec i))
        (loop (add1 i))))))

(define engine (make-default-engine))
(define db-options `((no-subdirectory? . #t)
                     (max-size . ,(* 10 1024 1024))))

(define (make-nstore)
  (nstore engine '(0) '(subject predicate object)))

(define (main json-path db-path)
  (let ((json (call-with-input-file json-path read-json))
        (store (make-nstore))
        (db (engine-open engine db-path db-options)))
    (engine-in-transaction engine db
      (lambda (tx)
        (for-each
         (lambda (item)
           (let* ((page-id (symbol->integer (car item)))
                  (attrs (cdr item))
                  (title (alist-ref 'title attrs))
                  (attrs (alist-delete 'title attrs)))
             (nstore-add! tx store (list page-id 'title title))
             (for-each
              (lambda (attr)
                (let ((key (car attr))
                      (values (cdr attr)))
                  (vector-for-each
                   (lambda (value)
                     (nstore-add! tx store (list page-id key value)))
                   values)))
              attrs)))
         json)))
    (engine-close engine db)))

(apply main (command-line-arguments))

)
