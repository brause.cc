BEGIN;

CREATE TABLE IF NOT EXISTS species (id INTEGER PRIMARY KEY,
                                    page_id INTEGER,
                                    title TEXT,
                                    UNIQUE(page_id));

CREATE TABLE IF NOT EXISTS attrs(id INTEGER PRIMARY KEY,
                                 key TEXT,
                                 value TEXT,
                                 UNIQUE(key, value));

CREATE TABLE IF NOT EXISTS species_attrs(species_id INTEGER,
                                         attr_id INTEGER,
                                         FOREIGN KEY(species_id) REFERENCES species(id),
                                         FOREIGN KEY(attr_id) REFERENCES attrs(id));

COMMIT;
