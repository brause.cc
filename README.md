Most of the demos listed below are written in [CHICKEN Scheme].

## brause.cc

[Animated CSS3 pixel art].  The effect is created by abusing the
`box-shadow` CSS attribute to attach a multitude of blocky shadows to
the `<div>` container and animating its value.

## animu.brause.cc

Atom feed for currently airing anime, powered by [AniDB].

## c4.brause.cc

Status page for the [CCCC] hackerspace, based on [their official API].
If you find this information useful, use the API instead of scraping
my page or you'll get blocked.

## depp.brause.cc

Statically generated Git repository views for my own code.

## elpa.brause.cc

[Statically generated Atom feeds] for new [ELPA packages].  Highly
useful to keep track of new Emacs packages to brag about!

## wald.brause.cc

Mushroom identification using Wikipedia [mycomorphboxes].

## x32.be/\*

[Ersatz-Dropbox].  I really like the permalink feature of [Dropbox],
so I decided to recreate it with a [rsync script].  To get the links,
I use a [Ruby script].

[CHICKEN Scheme]: https://call-cc.org/
[Animated CSS3 pixel art]: index.html
[AniDB]: https://anidb.net/
[CCCC]: https://koeln.ccc.de/
[their official API]: https://api.koeln.ccc.de/
[Statically generated Atom feeds]: elpa/elpa.scm
[ELPA]: https://www.gnu.org/software/emacs/manual/html_node/emacs/Packages.html
[mycomorphboxes]: https://en.wikipedia.org/wiki/Template:Mycomorphbox
[Ersatz-Dropbox]: https://x32.be/dealwithit.jpg
[Dropbox]: https://www.dropbox.com/
[rsync script]: /dotfiles/home/wasa/bin/fallkiste
[Ruby script]: /dotfiles/home/wasa/bin/permalink
